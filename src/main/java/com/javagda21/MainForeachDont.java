package com.javagda21;

import java.util.*;

public class MainForeachDont {
    public static void main(String[] args) {

        Set<String> stringSet = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h"));
        List<String> stringList = new ArrayList<>(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h"));


        // usuwanie z Set pętlą foreach generuje błąd "Concurrent Modification Exception"
        // w trakcie iteracji nie powinno się modyfikować zawartości zbioru
        for (String s : stringSet) {
            if (s.equalsIgnoreCase("h")) {
                stringSet.remove(s);
//                break;
            }
        }

        // rozwiązaniem problemu "concurrent modification exception" może być
        // użycie iteratora.
        // iterator to obiekt wskazujący na iterowany element w kolekcji

        // metoda hasNext - informuje o tym czy są dalej jakieś elementy
        // metoda next - przechodzi do kolejnego elementu
        Iterator<String> iterator = stringSet.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next(); // next przechodzi do następnego elementu

            if (element.equalsIgnoreCase("b")) {
                iterator.remove(); // usuwanie elementu
            }
        }

        // To samo dzieje się przy listach, nie wolno iterować foreach i usuwać w trakcie iteracji
        for (String s : stringList) {
            if (s.equalsIgnoreCase("b")) {
                stringList.remove(s);
            }
            if (s.equalsIgnoreCase("e")) {
                stringList.remove(s);
            }
        }

        // dla list najprostszym rozwiązaniem jest zastosowanie pętli fori
        for (int i = 0; i < stringList.size(); i++) {
            if (stringList.get(i).equalsIgnoreCase("b")) {
                stringList.remove(stringList.get(i));
                i--; // po usunięciu cofamy się o jeden indeks wstecz.
            }
        }

        System.out.println("Komunikat! Jupi!");
    }
}
