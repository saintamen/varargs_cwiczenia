package com.javagda21;

import java.util.Optional;

public class MainDziennik {
    public static void main(String[] args) {
        Dziennik dziennik = new Dziennik();

        dziennik.dodajStudentów(
                new Student("a", "a11", "1"),
                new Student("a", "a12", "2"),
                new Student("a", "a13", "3"),
                new Student("a", "a14", "4"),
                new Student("a", "a15", "5"),
                new Student("a", "a16", "6"),
                new Student("a", "a17", "7"),
                new Student("a", "a18", "8"),
                new Student("a", "a19", "9"),
                new Student("a", "a10", "10"),
                new Student("a", "a11", "11")
        );

        Optional<Student> optionalStudent = dziennik.znajdzStudenta("9");
        if (optionalStudent.isPresent()) { // zawsze przy optional zróbmy 'isPresent'
            Student studencik = optionalStudent.get();
            System.out.println("Znalazłem: " + studencik.getImie() + " o nazwisku: " + studencik.getNazwisko());
        }
        System.out.println(optionalStudent);

        dziennik.dodajOceny("1", 5.0, 4.3, 5.1, 3.0, 2.9, 3.5);
        dziennik.dodajOceny("2", 1.0, 4.3, 2.6, 6.4, 7.4, 5.6);
        dziennik.dodajOceny("3", 3.0, 3.2, 4.6, 5.9, 1.6, 2.7);
        System.out.println(dziennik.zwrocSredniaStudentów("1", "2", "3"));

        System.out.println(dziennik.zwrocPosortowanychPoIndeksie(true));
        System.out.println();
        System.out.println(dziennik.zwrocPosortowanychPoIndeksie(false));
    }
}
