package com.javagda21;

import com.javagda21.comparator.StudentComparator;

import java.util.*;

public class Dziennik {
    private List<Student> studentList = new ArrayList<>();

    public void dodajStudenta(Student student) {
        studentList.add(student);
    }

    // varargs
    public void dodajStudentów(Student... students) {
        for (Student s : students) {
            studentList.add(s);
        }
    }

    public void usunStudenta(Student s) {
        studentList.remove(s);
    }

    public void usunStudenta(Student... students) {
        for (Student s : students) {
            studentList.remove(s);
        }
    }

    public void usunStudenta(String indeksStudenta) {
        for (Student student : studentList) {
            // w trakcie iteracji sprawdzamy czy student (każdy na liście) ma szukany indeks
            if (student.getIndeks().equalsIgnoreCase(indeksStudenta)) {
                // znaleźliśmy studenta z podanym numerem indeksu
                studentList.remove(student);
                return;
            }
        }
    }

    public void usunStudenta(String... indeksyStudentow) {
        for (Student student : studentList) {
            for (int i = 0; i < indeksyStudentow.length; i++) {
                if (student.getIndeks().equalsIgnoreCase(indeksyStudentow[i])) {
                    // znaleźliśmy studenta z podanym numerem indeksu
                    studentList.remove(student);
                    break; // spowoduje wyjście z pętli wewnętrznej (iterowanie indeksyStudentow)(linia 43)
                }
            }
        }
    }

    // TODO: znajdź studenta (po podanym numerze indeksu)
    //  co zwracamy kiedy nie odnajdziemy studenta? :O     O.o

    public Optional<Student> znajdzStudenta(String numerIndeksu) {
        Student studentZwracany = null;
        for (Student student : studentList) {
            if (student.getIndeks().equalsIgnoreCase(numerIndeksu)) {
                studentZwracany = student;
                break;
            }
        }

//        return new Student("niema", "niema", "niema");
        return Optional.ofNullable(studentZwracany);
    }

    public List<Student> znajdzStudenta(String... numeryIndeksu) {
        List<Student> studenciZwracani = new ArrayList<>();
        for (Student student : studentList) {
            for (int i = 0; i < numeryIndeksu.length; i++) {
                if (student.getIndeks().equalsIgnoreCase(numeryIndeksu[i])) {
                    studenciZwracani.add(student);
//                    break;
                }
            }
        }

        return studenciZwracani;
    }

    public Optional<Double> zwrocSredniaStudenta(String numerIndeksu) {
        Optional<Student> studentOptional = znajdzStudenta(numerIndeksu);

        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            double suma = 0.0;
            for (Double ocena : student.getOceny()) {
                suma += ocena;
            }

            // opakowujemy wartość w optional.
            return Optional.of(suma / student.getOceny().size());
        }

        // co jeśli nie znalazłem studenta?
        return Optional.empty();
    }

    /**
     * Zwraca srednie szukanych studentow.
     *
     * @param numeryIndeksów - zmienna ilosc indeksów podana w parametrze.
     * @return - mapę indeks -> średnią
     */
    public Map<String, Optional<Double>> zwrocSredniaStudentów(String... numeryIndeksów) {
        Map<String, Optional<Double>> indeksyISrednie = new HashMap<>();
        for (String indeks : numeryIndeksów) {
            Optional<Double> optionalDouble = zwrocSredniaStudenta(indeks);

            indeksyISrednie.put(indeks, optionalDouble);
        }

        return indeksyISrednie;
    }

    public void dodajOceny(String indeksStudenta, Double... oceny) {
        Optional<Student> studentOptional = znajdzStudenta(indeksStudenta);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            student.getOceny().addAll(Arrays.asList(oceny));
        }
    }

    public List<Student> podajZagrozonych() {
        List<Student> students = new ArrayList<>();
        for (Student student : studentList) {
            Optional<Double> sredniaOptional = zwrocSredniaStudenta(student.getIndeks());

            if (sredniaOptional.isPresent()) {
                Double srednia = sredniaOptional.get();
                if (!srednia.isNaN()) { // jeśli student nie ma ocen wynik będzie NaN
                    students.add(student);
                }
            }
        }

        return students;
    }

    public List<Student> zwrocPosortowanychPoIndeksie(boolean czyRosnaco) {
        Collections.sort(studentList, new StudentComparator(czyRosnaco));
        return studentList;
    }
}
