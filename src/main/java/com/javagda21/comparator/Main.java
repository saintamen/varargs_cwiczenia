package com.javagda21.comparator;

import com.javagda21.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        List<Integer> studenty = new ArrayList<>(Arrays.asList(1, 2, 5, 190, 3, 2, 125, 45, 2134, 23, 1, 2, 5, 46, 657, 6));
        List<Student> studenty = new ArrayList<>(Arrays.asList(
                new Student("a", "a19", "9"),
                new Student("a", "a15", "5"),
                new Student("a", "a11", "1"),
                new Student("a", "a13", "3"),
                new Student("a", "a16", "6"),
                new Student("a", "a14", "4"),
                new Student("a", "a11", "11"),
                new Student("a", "a18", "8"),
                new Student("a", "a12", "2"),
                new Student("a", "a10", "10"),
                new Student("a", "a17", "7")
        ));

//        StudentComparator studentComparator = new StudentComparator(true);
//        Collections.sort(studenty, studentComparator);
//
        Collections.sort(studenty, new StudentComparator(true));


        System.out.println(studenty);
    }
}
