package com.javagda21.comparator;

import com.javagda21.Student;

import java.util.Comparator;

public class StudentComparator implements Comparator<Student> {
    private boolean czyRosnaco;

    public StudentComparator(boolean czyRosnaco) {
        this.czyRosnaco = czyRosnaco;
    }

    @Override
    public int compare(Student o1, Student o2) {

        int indekso1 = Integer.parseInt(o1.getIndeks());
        int indekso2 = Integer.parseInt(o2.getIndeks());
        if (indekso1 > indekso2) {
            return -1 * (czyRosnaco ? 1 : -1);
        } else if (indekso2 > indekso1) {
            return 1 * (czyRosnaco ? 1 : -1);
        }

        // kolejny if

        return 0;
    }
}
