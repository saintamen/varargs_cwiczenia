package com.javagda21.comparator;

import com.javagda21.Student;

import java.util.Comparator;

public class ComparatorTekstowyStudenta implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {

        // compareTo w klasie String:
        // zwróci -1/1/0 w zależności który string jest alfabetycznie
        // 'wyżej/niżej'

        // to poniżej nie zadziała
//        if(o1.getNazwisko() > o2.getNazwisko()){
//
//        }

        return o1.getNazwisko().compareTo(o2.getNazwisko());
    }
}
