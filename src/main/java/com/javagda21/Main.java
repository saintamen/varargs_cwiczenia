package com.javagda21;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // varargs
        // variable number of arguments

        // Arrays.asList(); - < wpisuje elementy (parametry) do listy
        doListy(1, 2, 3, 4, 5, 6, 6);

    }

    public static <T> List<T> doListy(T... arguments) { // <- arguments jest postrzegane jako tablica
        // 1 tworzymy listę
        List<T> list = new ArrayList<>();

        // dodajemy wszystkie elementy z argumentu
        for (int i = 0; i < arguments.length; i++) {
            list.add(arguments[i]);
        }

        // zwracamy listę
        return list;
    }

}
