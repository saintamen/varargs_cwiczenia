package com.javagda21;

import java.util.ArrayList;
import java.util.List;

//public class Student implements Comparable<Student> { // ja student mogę porównać się do innego studenta
    public class Student {
    private String imie;
    private String nazwisko;
    private String indeks;
    private List<Double> oceny = new ArrayList<>();

    public Student() {
    }

    public Student(String imie, String nazwisko, String indeks) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.indeks = indeks;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getIndeks() {
        return indeks;
    }

    public void setIndeks(String indeks) {
        this.indeks = indeks;
    }

    public List<Double> getOceny() {
        return oceny;
    }

    public void setOceny(List<Double> oceny) {
        this.oceny = oceny;
    }

    @Override
    public String toString() {
        return "Student{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", indeks='" + indeks + '\'' +
                ", oceny=" + oceny +
                '}';
    }

    // mniejsze = -1
    // równe = 0
    // większe = 1
//    @Override
    public int compareTo(Student o) {
//        this <- my, ten obiekt
//        o <- drugi obiekt, do którego porównujemy 'this'
        int indeksMój = Integer.parseInt(this.indeks);
        int indeksGoscia = Integer.parseInt(o.indeks);
        if (indeksMój > indeksGoscia) {
            return 1;
        } else if (indeksMój < indeksGoscia) {
            return -1;
        }
        return 0;
    }
}
