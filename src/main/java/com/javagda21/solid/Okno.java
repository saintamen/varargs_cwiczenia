package com.javagda21.solid;

public class Okno implements IDrzwi {
    @Override
    public void otworz() {

    }

    @Override
    public void zamknij() {

    }

    @Override
    public void zaklucz() {

    }

    @Override
    public void wyważyć() {

    }

    @Override
    public void odklucz() {
        throw new UnsupportedOperationException();
    }
}
