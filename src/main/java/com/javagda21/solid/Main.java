package com.javagda21.solid;

import java.util.ArrayList;
import java.util.List;

public class Main {

    // S - Single Responsibility - zasada pojedynczej odpowiedzialnosci.
    //  Klasa/metoda powinna odpowiadac/robić jedną rzecz.
    // O - Open/Close principle - otwarty na rozszerzenia i zamknięty na modyfikacje
    //
    // L - Liskov principle - liskov substitution - po lewej możliwie najwyższą klasę
    //
    // I - Interface segregation - powinniśmy tworzyć możliwie najwięcej prostych interfejsów
    //
    // D - Dependency inversion -
    //      Dependency Injection


    // KlasaA
    //     KlasaADziedzicz

    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();

//        integerList.add(5);
    }
}
