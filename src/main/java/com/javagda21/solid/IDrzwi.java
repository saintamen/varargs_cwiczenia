package com.javagda21.solid;

public interface IDrzwi {
    void otworz();
    void zamknij();
    void zaklucz();
    void wyważyć();
    void odklucz();
}
